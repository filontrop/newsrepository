//
//  APIManager.swift
//  myInstagram
//
//  Created by Fil  on 5.01.23.
//

import Foundation

var News: [New] = []

func loadNews(complitionHandler: (() -> Void)?){
    let url = URL(string: "https://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=4c4d366498a74582abe79b74c0d285e3")
    let session = URLSession(configuration: .default)
    let dounloadTask = session.downloadTask(with: url!) { (urlFile, response, error) in
        guard let urlFile = urlFile else { fatalError("file dont loded")}
        let path = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true)[0] + "/data.json"
        let urlPath = URL(fileURLWithPath: path)
        try? FileManager.default.copyItem(at: urlFile, to: urlPath) // catch
    parseNews(path: path, urlPath: urlPath,url: url!)
        complitionHandler?()
    }
    
    dounloadTask.resume()
}


func parseNews(path: String, urlPath: URL,url: URL){
    guard let data = try? Data(contentsOf: urlPath) else {fatalError("data dont loded")}
    guard let loadedDictionary =  try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? Dictionary<String, Any> else {
        fatalError("Dictionary dont loded") }
    guard let newsArray = loadedDictionary["articles"] as? [Dictionary<String, Any>] else {
        fatalError("articles dont loded") }
    for element in newsArray{
        let new = New(dictionary: element)
        News.append(new)
    }
    
}
