//
//  NewsModel.swift
//  myInstagram
//
//  Created by Fil  on 5.01.23.
//

import Foundation

struct New {
    var author: String
    var title: String
    var url: String
    var urlToImage: String
    var publishedAt: String
    var content: String
    
    init(dictionary: Dictionary<String, Any>){
        author = dictionary["author"] as? String ?? ""
        title = dictionary["title"]  as? String ?? ""
        url = dictionary["url"]  as? String ?? ""
        urlToImage = dictionary["urlToImage"]  as? String ?? ""
        publishedAt = dictionary["publishedAt"]  as? String ?? ""
        content = dictionary["content"]  as? String ?? ""
    }
}
