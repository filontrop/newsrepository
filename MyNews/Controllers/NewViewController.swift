//
//  NewViewController.swift
//  myInstagram
//
//  Created by Fil  on 10.01.23.
//

import UIKit
import SafariServices

class NewViewController: UIViewController {
    
    @IBOutlet weak var openButton: UIButton!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var newImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var newActivityIndicator: UIActivityIndicatorView!
    var new: New!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView(){
        self.newImage.layer.cornerRadius = 15
        contentLabel.text = new.content
        titleLabel.text = new.title
        newActivityIndicator.startAnimating()
        newActivityIndicator.hidesWhenStopped = true
        
        DispatchQueue.global().async{
            guard let url = URL(string: self.new.urlToImage) else {
                DispatchQueue.main.sync {
                    self.newImage.image = UIImage(named: "no_image")
                    self.newActivityIndicator.stopAnimating()
                }
                return
            }
            guard let data = try? Data(contentsOf: url) else { return }
            
            DispatchQueue.main.sync {
                self.newImage.image = UIImage(data: data)
                self.newActivityIndicator.stopAnimating()
            }
        }
    }
    // MARK: - Actions
    @IBAction func lernMoreTouchedUpInside(_ sender: Any) {
        if let newUrl = URL(string: new.url){
            let svc = SFSafariViewController(url: newUrl)
            present(svc, animated: true)
        } else {
            openButton.isHidden = true
        }
    }
}


