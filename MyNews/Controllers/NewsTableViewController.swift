//
//  InstagramTableViewController.swift
//  myInstagram
//
//  Created by Fil  on 5.01.23.
//

import UIKit

class NewsTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        loadNews{
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    // MARK: - TableViewDataSource

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return News.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! CustomTableViewCell
        cell.infoImage.layer.cornerRadius = 15
        cell.infoLabel.text = News[indexPath.row].title
        cell.imageActivityIndicator.hidesWhenStopped = true
        cell.publishedAtLable.text = News[indexPath.row].publishedAt
        
        DispatchQueue.global().async{
            guard let url = URL(string: News[indexPath.row].urlToImage) else {
                DispatchQueue.main.sync {
                    cell.imageActivityIndicator.stopAnimating()
                    cell.infoImage.image = UIImage(named: "no_image")
                }
                return
            }
            guard let data = try? Data(contentsOf: url) else { return }
            
            
            DispatchQueue.main.sync {
                cell.infoImage.image = UIImage(data: data)
                cell.imageActivityIndicator.stopAnimating()
               
            }
        }
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "newSegue"){
            if let indexPath =  sender { 
                let detailVC = segue.destination as! NewViewController
                detailVC.new = News[(indexPath as AnyObject).row]
                
            }
        }
    }
    
    // MARK: - TableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
            performSegue(withIdentifier: "newSegue", sender: indexPath)
    }
}

